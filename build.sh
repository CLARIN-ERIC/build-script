#!/usr/bin/env bash
# shellcheck disable=SC1091
set -e

BUILD_IMAGE="registry.gitlab.com/clarin-eric/build-image:1.3.5"
SCAN_IMAGE="registry.gitlab.com/clarin-eric/docker-snyk-cli:0.0.9"
DOCKER_LINT_IMAGE="hadolint/hadolint:2.12.0-alpine"
SHELL_LINT_IMAGE="koalaman/shellcheck-alpine:v0.9.0"
QEMU_USER_STATIC_IMAGE="multiarch/qemu-user-static:7.2.0-1"
DEFAULT_PLATFORMS="linux/amd64,linux/arm64"

#
# Set default values for parameters
#
MODE="gitlab"
BUILD=0
TEST=0
RUN=0
RELEASE=0
VERBOSE=0
NO_EXPORT=0
DOWN=0
SCAN=0
LINT_DOCKER=0
LINT_SHELL=0
NO_CACHE=0

#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -b|--build)
        BUILD=1
        ;;
    -c|--no-cache)
        NO_CACHE=1
        ;;
    -d|--down)
        DOWN=1
        ;;
    -s|--scan)
        SCAN=1
        ;;
    -ld|--lint-docker)
        LINT_DOCKER=1
        ;;
    -ls|--lint-shell)
        LINT_SHELL=1
        ;;
    -h|--help)
        MODE="help"
        ;;
    -l|--local)
        MODE="local"
        ;;
    -n|--no-export)
        NO_EXPORT=1
        ;;
    -r|--release)
        RELEASE=1
        ;;
    -t|--test)
        TEST=1
        ;;
    -R|--run)
        RUN=1
        ;;
    -v|--verbose)
        VERBOSE=1
        ;;
    *)
        echo "Unkown option: $key"
        MODE="help"
        ;;
esac
shift # past argument or value
done

# Source variables if it exists
if [ -f variables.sh ]; then
    if  [ "${NO_EXPORT}" -eq 0 ]; then
        echo "Overriding configuration"
        set -x
    fi

    . ./variables.sh
    set +x
fi

_IMAGE_DIR=${IMAGE_DIR:-"image/"}


# Print parameters if running in verbose mode
if [ ${VERBOSE} -eq 1 ]; then
    echo "build=${BUILD}"
    set -x
fi

source ./copy_data.sh
source ./update_version.sh

#
# Force local mode if run or down commands are specified
#
if [ ${RUN} -eq 1 ] || [ ${DOWN} -eq 1 ]; then
    if [ "${MODE}" != "local" ]; then
        echo "Forcing local mode"
        MODE="local"
    fi
fi

#
# Execute based on mode argument
#
if [ ${MODE} == "help" ]; then
    echo ""
    echo "build.sh [-bdhlnRrtvs(ld)(ls)]"
    echo ""
    echo "  -b,  --build        Build docker image"
    echo "  -r,  --release      Push docker image to registry"
    echo "  -t,  --test         Execute tests"
    echo "  -ld, --lint-docker  Lint Dockerfile under <image directory>/Dockerfile"
    echo "  -ls, --lint-shell   Lint shell scripts under <image directory>/[*.sh, *.bash]"
    echo "  -s,  --scan         Perform static vulnerability scanning on image"
    echo "                       Make sure the SNYK_TOKEN variable is set (export SNYK_TOKEN=<token value>)"
    echo "  -R,  --run          Run (only works in local (-l, --local) mode)"
    echo "  -d,  --down         Down (only works in local (-l, --local) mode)"
    echo ""
    echo "  -c,  --no-cache     Ignore docker cache during build (-b) step"
    echo "  -l,  --local        Run workflow locally in a local docker container"
    echo "  -v,  --verbose      Run in verbose mode"
    echo "  -n,  --no-export    Don't export the build artiface, this is used when running"
    echo "                       the build workflow locally"
    echo ""
    echo "  Environment variables:"
    echo "      BUILD_PLATFORMS: Docker buildx target platforms. (Default:\"linux/amd64,linux/arm64\")"
    echo ""
    echo "  -h,  --help         Show help"
    echo ""
    exit 0
elif [ "${MODE}" == "gitlab" ]; then
    git config --global --add safe.directory "$PWD"

    if [ -n "$CI_SERVER" ]; then
        TAG="${CI_COMMIT_TAG:-$CI_COMMIT_SHA}"
        IMAGE_QUALIFIED_NAME="${CI_REGISTRY_IMAGE}:${TAG}"
        IMAGE_FILE_NAME="${CI_REGISTRY_IMAGE##*/}:${TAG}"
    else
        # WARNING: The current working dir must equal the project root dir.

        PROJECT_NAME="$(basename "$(pwd)")"
        TAG="$(git describe --always)"
        IMAGE_QUALIFIED_NAME="$PROJECT_NAME:${TAG:-latest}"
        IMAGE_FILE_NAME="${IMAGE_QUALIFIED_NAME}"
    fi

    #Create output directory on-the-fly
    if [ ! -d './output' ]; then
        mkdir -p 'output'
    fi

    IMAGE_OUT_PATH="$(readlink -fn './output/')"
    IMAGE_FILE_PATH="${IMAGE_OUT_PATH}/$IMAGE_FILE_NAME.tar.gz"
    IMAGE_CACHE_PATH="${IMAGE_OUT_PATH}/cache"
    export IMAGE_QUALIFIED_NAME
    export IMAGE_FILE_PATH
    export IMAGE_CACHE_PATH

    PROJ_BASE_DIR="$(pwd)"

    #Copy data
    if [ "${BUILD}" -eq 1 ] || [ "${TEST}" -eq 1 ] || [ "${RELEASE}" -eq 1 ]; then
        echo "**** Copying data ****"
        cd -- "${_IMAGE_DIR}"
        if  [ "${NO_EXPORT}" -eq 0 ]; then
            init_data
        fi

        cd "${PROJ_BASE_DIR}"
    fi

    #Lint Dockerfile
    if [ "${LINT_DOCKER}" -eq 1 ]; then
        echo "**** Linting Dockerfile ****"
        echo "Image: ${IMAGE_QUALIFIED_NAME}"

        docker run --rm -v "/var/run/docker.sock:/var/run/docker.sock" \
            -v "$(pwd):/project" \
            "${DOCKER_LINT_IMAGE}" \
            sh -c "hadolint /project/${_IMAGE_DIR}/Dockerfile"

        EXIT_CODE=$?
        if [ "${EXIT_CODE}" -eq 0 ]; then
            echo "**** Success ****"
        else
            echo "**** Fail ****"
        fi

        exit $EXIT_CODE
    fi

    #Lint Shell code
    if [ "${LINT_SHELL}" -eq 1 ]; then
        echo "**** Linting image shell scripts ****"
        echo "Image: ${IMAGE_QUALIFIED_NAME}"

        #Source linting setup
        if [ -f ".linting" ]; then
          source .linting set
        fi

        #Set linting prune options
        LINT_PRUNE_OPTS=""
        if [ "${LINT_SHELL_EXCLUDE_DIRS}" != "" ]; then
          dirs=""
          IFS=";"
          for dir_to_exclude in ${LINT_SHELL_EXCLUDE_DIRS}; do
            printf "dir_to_exclude: %s\n" "${dir_to_exclude}"
            if [ "${dirs}" == "" ]; then
              dirs="-name \"${dir_to_exclude}\""
            else
              dirs="${dirs} -o -name \"${dir_to_exclude}\""
            fi
          done
          LINT_PRUNE_OPTS="-type d \\( ${dirs} \\) -prune"
        fi

        set +e
        docker run --rm -v "/var/run/docker.sock:/var/run/docker.sock" \
            -v "$(pwd):/project" \
            "${SHELL_LINT_IMAGE}" \
                sh -c "
                    echo 0 >/tmp/exitcode
                    find /project/ ${LINT_PRUNE_OPTS} \( -name \"*.sh\" -o -name \"*.bash\" \) -exec sh -c 'echo \*\* Linting: \$1 \*\*; if ! shellcheck \$1 --color=always --exclude SC1091; then echo 1 >>/tmp/exitcode; fi; echo \*\* Done \*\*' sh {} \;
                    return \$(awk '{ sum += \$1 } END { print sum }' /tmp/exitcode);"
        EXIT_CODE=$?
        set -e
        if [ "${EXIT_CODE}" -eq 0 ]; then
            echo "**** Success ****"
        else
            echo "**** FAILED lint for ${EXIT_CODE} scripts"
            echo "**** Fail ****"
        fi

        exit "$EXIT_CODE"
    fi

    #Build
    if [ "${BUILD}" -eq 1 ]; then

        echo "**** Building image ****"
        
        cd -- "${_IMAGE_DIR}"
        
        #Build docker build arguments
        DOCKER_ARGS=()
        if [[ -n "${GITLAB_DOCKER_ARGS}" ]]; then
            DOCKER_ARGS+=("${GITLAB_DOCKER_ARGS}")
        fi
        DOCKER_ARGS+=("--tag=$IMAGE_QUALIFIED_NAME")
        if [[ -n ${DIST_VERSION+x} ]]; then
            DOCKER_ARGS+=("--build-arg" "DIST_VERSION=${DIST_VERSION}")
        fi

        if [ "${NO_CACHE}" -eq 1 ]; then
            DOCKER_ARGS+=("--no-cache")
        fi

        update_version_before "${TAG}"

        BUILDX_ARGS=( "--progress" "plain" "--load" )

        if  [ "${NO_EXPORT}" -eq 0 ]; then
            #Only try to use last CI image as cache when running in a gitlab CI pipeline
            if [ -z "${CI_COMMIT_TAG}" ]; then
                LATEST_REGISTRY_IMG="${CI_REGISTRY_IMAGE}:$(git tag |tail -1)"
            else
                #Use previous tag when build a new tag
                LATEST_REGISTRY_IMG="${CI_REGISTRY_IMAGE}:$(git tag |tail -2 |head -1)"
            fi
            docker pull "$LATEST_REGISTRY_IMG" || true

            #Only export build cache to disk when running in a gitlab CI pipeline
            BUILDX_ARGS+=("--cache-from" "type=registry,ref=${LATEST_REGISTRY_IMG}" \
              "--cache-to=type=local,dest=${IMAGE_CACHE_PATH},mode=max")

            #Only enable cross compilation when running in a gitlab CI pipeline
            docker run --rm --privileged ${QEMU_USER_STATIC_IMAGE} --reset -p yes
        fi

        #Create a builder with TLS support
        docker context create tls-environment
        docker buildx create --name multi-arch-builder --driver docker-container --use tls-environment
        docker buildx inspect --bootstrap

        #Build and load single architecture image for the current platform
        if  [ "${NO_EXPORT}" -eq 1 ]; then
            #local build
            if ! docker buildx build "${BUILDX_ARGS[@]}" "${DOCKER_ARGS[@]}" .; then
                WARN_COLOR='\033[0;33m'
                NO_COLOR='\033[0m'
                echo -e "\n\n${WARN_COLOR}WARNING${NO_COLOR}: Could not download FROM image from remote registry"
                echo "  -> trying to use local images via the default docker buildx \"docker\" driver"
                echo "  This local build won't use the same buildx driver as the CLARIN GitLab CI."
                echo "  The CI pipeline will fail if the current sate is pushed to GitLab without"
                echo -e "  the FROM image available on a remote registry.\n\n"
                docker buildx use default
                docker buildx build "${BUILDX_ARGS[@]}" "${DOCKER_ARGS[@]}" .
            fi
        else
            #remote (gitlab ci) build
            docker buildx build "${BUILDX_ARGS[@]}" "${DOCKER_ARGS[@]}" .
        fi
        update_version_before "${TAG}"

        if  [ "${NO_EXPORT}" -eq 0 ]; then
            #Only export artifact to disk when running in a gitlab CI pipeline
            docker save --output="$IMAGE_FILE_PATH" "$IMAGE_QUALIFIED_NAME"
        fi
            
        #Clean up data
        if [ "${TEST}" -eq 0 ]; then
            echo "**** Cleaning up data ****"
            if  [ "${NO_EXPORT}" -eq 0 ]; then
                cleanup_data
            fi
        fi
    fi

    #Scan
    if [ "${SCAN}" -eq 1 ]; then
        echo "**** Static vulnerability scanning ****"
        echo "Image: ${IMAGE_QUALIFIED_NAME}"

        #Load image in gitlab CI pipeline
        if  [ "${NO_EXPORT}" -eq 0 ]; then
            docker load --input="$IMAGE_FILE_PATH"
        fi

        if [ -z ${SNYK_TOKEN+x} ]; then
          echo "SNYK_TOKEN not found. Set the variable via: export SNYK_TOKEN=<token value>"
          exit 1
        fi

        #If the SNYK_POLICY_FILE variable is set and the file exists at "/project/${SNYK_POLICY_FILE}" enable this
        #policy file and pass it to the snyk test command
        POLICY=""
        if [ "${SNYK_POLICY_FILE}" != "" ]; then
          if [ -f "${SNYK_POLICY_FILE}" ]; then
            echo "Enabling snyk policy file (${SNYK_POLICY_FILE})"
            POLICY="--policy-path=/project/${SNYK_POLICY_FILE}"
          fi
        fi

        docker run --rm -e "SNYK_TOKEN=${SNYK_TOKEN}" \
          -e "SCAN_SEVERITY_THRESHOLD=${SCAN_SEVERITY_THRESHOLD}" \
          -e "SCAN_FAIL_ON=${SCAN_FAIL_ON}" \
          -v "$(pwd):/project" \
          -v "/var/run/docker.sock:/var/run/docker.sock" \
          "${SCAN_IMAGE}" \
            container test "${IMAGE_QUALIFIED_NAME}" --file="/project/${_IMAGE_DIR}/Dockerfile" "${POLICY}"

        EXIT_CODE=$?
        if [ "${EXIT_CODE}" -eq 0 ]; then
            echo "**** Success ****"
        else
            echo "**** Fail ****"
        fi

        exit $EXIT_CODE
    fi

    #Test
    if [ "${TEST}" -eq 1 ]; then
        echo "**** Testing image ****"
        if [ ! -d 'test' ]; then
            echo "Test directory (./test/) not found"
            exit 1
        fi
        if [ ! -f 'test/docker-compose.yml' ]; then
            echo "docker-compose.yml not found in test directory (./test/docker-compose.yml)"
            exit 1
        fi
        cd -- 'test/'
        #Load image in gitlab CI pipeline
        if  [ "${NO_EXPORT}" -eq 0 ]; then
            docker load --input="$IMAGE_FILE_PATH"
        fi
        #cleanup to ensure clean state
        docker-compose down -v
        #Start services
        docker-compose up
        #Verify all containers are closed nicely
        number_of_failed_containers="$(docker-compose ps -q | xargs docker inspect \
            -f '{{ .State.ExitCode }}' | grep -c 0 -v | tr -d ' ')"
        #cleanup
        docker-compose down -v
        
        echo "**** Cleaning up data ****"
        cd "${PROJ_BASE_DIR}" && cd -- "${_IMAGE_DIR}"
        if  [ "${NO_EXPORT}" -eq 0 ]; then
            cleanup_data
        fi
        
        #return result
        exit "$number_of_failed_containers"
    fi

    if [ "${RUN}" -eq 1 ]; then
        echo "Run is not supported in gitlab mode"
    fi

    #Release
    if [ "${RELEASE}" -eq 1 ]; then
        echo "**** Releasing image ****"
        
        cd -- "${_IMAGE_DIR}"

        echo "${CI_JOB_TOKEN}" | docker login -u 'gitlab-ci-token' --password-stdin 'registry.gitlab.com'

        #Build docker build arguments
        DOCKER_ARGS=()
        if [[ -n "${GITLAB_DOCKER_ARGS}" ]]; then
            DOCKER_ARGS+=("${GITLAB_DOCKER_ARGS}")
        fi
        DOCKER_ARGS+=("--tag=$IMAGE_QUALIFIED_NAME")
        if [[ -n "${DIST_VERSION+x}" ]]; then
            DOCKER_ARGS+=("--build-arg" "DIST_VERSION=${DIST_VERSION}")
        fi
        if [[ -z "${BUILD_PLATFORMS}" ]]; then
            BUILD_PLATFORMS="${DEFAULT_PLATFORMS}"
        fi
        BUILDX_ARGS=("--progress" "plain" \
          "--tag=$IMAGE_QUALIFIED_NAME" \
          "--platform" "${BUILD_PLATFORMS}" \
          "--push" \
          "--cache-from" "type=local,src=${IMAGE_CACHE_PATH}")

        # Enable cross compilation
        docker run --rm --privileged ${QEMU_USER_STATIC_IMAGE} --reset -p yes

        #Create a TLS builder
        docker context create tls-environment
        docker buildx create --name multi-arch-builder --driver docker-container --use tls-environment
        docker buildx inspect --bootstrap

        #Build and push multi-architecture image using cache from build stage
        docker buildx build \
            "${BUILDX_ARGS[@]}" "${DOCKER_ARGS[@]}" .
                    
        echo "**** Cleaning up data ****"
        cd "${PROJ_BASE_DIR}" && cd -- "${_IMAGE_DIR}"
        if  [ "${NO_EXPORT}" -eq 0 ]; then
            cleanup_data
        fi
        
        docker logout 'registry.gitlab.com'
    fi

elif [ "${MODE}" == "local" ]; then

    if [ "${DOWN}" -eq 1 ]; then
        if [ ! -d 'run' ]; then
            echo "Run directory (./run/) not found"
            exit 1
        fi
        if [ ! -f 'run/docker-compose.yml' ]; then
            echo "docker-compose.yml not found in run directory (./run/docker-compose.yml)"
            exit 1
        fi
        PROJECT_NAME="$(basename "$(pwd)")"
        TAG="$(git describe --always)"
        IMAGE_QUALIFIED_NAME="$PROJECT_NAME:${TAG:-latest}"
        export IMAGE_QUALIFIED_NAME

        cd -- 'run/'
        #cleanup to ensure clean state
        docker-compose down -v
    fi

    #Run
    if [ "${RUN}" -eq 1 ]; then
        if [ ! -d 'run' ]; then
            echo "Run directory (./run/) not found"
            exit 1
        fi
        if [ ! -f 'run/docker-compose.yml' ]; then
            echo "docker-compose.yml not found in run directory (./run/docker-compose.yml)"
            exit 1
        fi
        PROJECT_NAME="$(basename "$(pwd)")"
        TAG="$(git describe --always)"
        IMAGE_QUALIFIED_NAME="$PROJECT_NAME:${TAG:-latest}"
        export IMAGE_QUALIFIED_NAME

        cd -- 'run/'
        #cleanup to ensure clean state
        docker-compose down -v
        #rebuild docker images
        docker-compose build
        #Start services
        docker-compose up
    else
        #
        # Setup all commands
        #

        SHELL_FLAGS=""
        if [ ${VERBOSE} -eq 1 ]; then
            SHELL_FLAGS="-x"
        fi

        FLAGS=""
        if [ ${NO_CACHE} -eq 1 ]; then
            FLAGS="${FLAGS}  --no-cache"
        fi

        CMD=""
        if [ ${LINT_SHELL} -eq 1 ]; then
            CMD="${CMD}bash ${SHELL_FLAGS} ./build.sh --lint-shell --no-export ${FLAGS}"
        fi
        if [ ${LINT_DOCKER} -eq 1 ]; then
            if [[ -n "${CMD}" ]]; then
                CMD="${CMD} && "
            fi
            CMD="${CMD}bash ${SHELL_FLAGS} ./build.sh --lint-docker --no-export ${FLAGS}"
        fi
        if [ ${BUILD} -eq 1 ]; then
            if [[ -n "${CMD}" ]]; then
                CMD="${CMD} && "
            fi
            CMD="${CMD}bash ${SHELL_FLAGS} ./build.sh --build --no-export ${FLAGS}"
        fi
        if [ ${TEST} -eq 1 ]; then
            if [[ -n "${CMD}" ]]; then
                CMD="${CMD} && "
            fi
           CMD="${CMD}bash ${SHELL_FLAGS} ./build.sh --test --no-export ${FLAGS}"
        fi
        if [ ${SCAN} -eq 1 ]; then
            if [[ -n "${CMD}" ]]; then
                CMD="${CMD} && "
            fi
            CMD="${CMD}bash ${SHELL_FLAGS} ./build.sh --scan --no-export ${FLAGS}"
        fi

        #
        # Build process
        #

        #Prepare environmt by downloading external resources when building an image
        if [ ${BUILD} -eq 1 ]; then
            cwd=$(pwd)
            cd -- "${_IMAGE_DIR}"
            cleanup_data
            init_data "local"
            cd -- "${cwd}"
        fi

        #Start the build process
        docker run \
            --volume='/var/run/docker.sock:/var/run/docker.sock' \
            --rm \
            --volume="$PWD":"$PWD" \
            --workdir="$PWD" \
            --env SNYK_TOKEN="$SNYK_TOKEN" \
            --env SCAN_SEVERITY_THRESHOLD="$SCAN_SEVERITY_THRESHOLD" \
            --env SCAN_FAIL_ON="$SCAN_FAIL_ON" \
            --env SNYK_POLICY_FILE="$SNYK_POLICY_FILE" \
            -it \
            ${BUILD_IMAGE} \
            bash -c "${CMD}"

        #Cleanup environment from downloaded resources when building an image
        if [ ${BUILD} -eq 1 ]; then
            cwd=$(pwd)
            cd -- "${_IMAGE_DIR}"
            cleanup_data
            cd -- "${cwd}"
        fi
    fi
else
    exit 1
fi
