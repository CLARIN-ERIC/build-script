#!/usr/bin/env bash

set -e

LATEST_VERSION="2.0.19"

HELP=0
VERSION=${LATEST_VERSION}

#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -v)
        VERSION=${2}
        shift
        ;;
    -h|--help)
        HELP=1
        ;;
    *)
        echo "Unkown option: $key"
        HELP=1
        ;;
esac
shift # past argument or value
done

help() {
    echo "Run ./init-repo.sh [-v <version>]"
    echo ""
    echo "Without any arguments this command will initialize a new clarin docker image git repository or update an existing"
    echo "repository to the latest version of the build script"
    echo ""
    echo "Arguments:"
    echo "  -v <version>        install the specified version instead. The supplied version must match an existing tag"
    echo ""
    exit 1
}

if [ "${HELP}" == "1" ]; then
    help
fi


INITIALIZED=0
set +e

if git rev-parse --is-inside-work-tree  > /dev/null 2>&1; then
    INITIALIZED=1
fi
set -e

if [ "${INITIALIZED}" == "0" ]; then
    echo "Initializing directory with build-script v${VERSION}"

    #TODO: update
    #   take into account custom copy_data.sh. When this file is a symlink don't replace it

    git init
    git submodule add https://gitlab.com/CLARIN-ERIC/build-script.git build-script
    cd build-script
    git checkout "${VERSION}"
    cd ..
    mkdir image run test
    ln -s build-script/build.sh build.sh
    ln -s build-script/init_repo.sh update_build_script.bash
    cp build-script/copy_data_noop.sh copy_data.sh
    cp build-script/update_version_noop.sh update_version.sh
    cp build-script/_gitlab-ci_default.yml .gitlab-ci.yml
    cp build-script/_docker-compose-test_default.yml test/docker-compose.yml
    cp build-script/_checker_default.conf test/checker.conf

    touch image/.gitkeep
    touch run/.gitkeep
    touch test/.gitkeep
    git add .
    git commit -m "Intialized empty repo with build script v${VERSION}"

    echo "Done"
else
    echo "Updating to build-script v${VERSION}"
    cd build-script
    #git checkout ${VERSION}
    git fetch origin --depth=1 "+refs/tags/${VERSION}:refs/tags/${VERSION}"
    git reset --hard "tags/${VERSION}"

    cd ..
    if [ ! -f update_build_script.bash ]; then
      ln -s build-script/init_repo.sh update_build_script.bash
    fi

    echo "Done"
    echo ""
    echo "  Changes:"
    echo "    .gitlab-ci.yml has changed, run \"diff .gitlab-ci.yml build-script/_gitlab-ci_default.yml\" to review the differences"
    echo ""
fi
